#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author : Xavier Bruni

from tkinter import *
from model import Model
from view import View
import json

class Controller():

	def __init__(self):
		self.root = Tk()
		self.model = Model()
		self.conf = json.loads(open("./config/apis.json", "r").read())
		self.model.connectBridge(self.conf)
		self.view = View(self.root, self.conf)
		self.view.selectedApi.trace("w", self.changeApi)
		self.view.sellButton.bind("<Button>", self.panicsell)
		self.view.configButton.bind("<Button>", self.config)

	def run(self):
		self.root.title("AutoLOL")
		self.root.deiconify()
		self.root.mainloop()

	def changeApi(self, *args):
		selectedApi = self.view.selectedApi.get()
		if(selectedApi != self.conf['selectedApi']):
			self.conf['selectedApi'] = selectedApi
			file = open("./config/apis.json", "w")
			file.write(json.dumps(self.conf))
			self.model.connectBridge(self.conf)
			self.view.logs.insert(END, "Changed API to "+selectedApi+"\n")

	def panicsell(self, event):
		self.view.logs.insert(END, "Fetching available cryptos ... ")
		self.root.update_idletasks()
		cryptos = self.model.getCryptos()
		self.view.logs.insert(END, "done\n")
		if(len(cryptos) <= 0):
			self.view.logs.insert(END, "No cryptos to sell found.\n")
			return
		self.sellWin = Toplevel(self.root)
		self.sellWin.grab_set()
		self.view.showBeforeSell(self.sellWin, cryptos)
		self.view.sellButton.bind("<Button>", self.confirmPanic)
		self.view.cancelButton.bind("<Button>", self.cancelPanic)

	def confirmPanic(self, event):
		curs = []
		for ck in self.view.checkBoxs:
			if(ck.var.get() == True):
				curs.append(ck.crypto)
		self.sellWin.destroy()
		self.view.logs.insert(END, "Starting market selling ... DO NOT TOUCH ANYTHING UNTIL AUTHORIZED to avoid conflict.\n")
		self.root.update_idletasks()
		self.model.panicSell(curs, self.curRet)
		self.view.logs.insert(END, "Panic sell finished. You can resume your activity\n")

	def cancelPanic(self, event):
		self.sellWin.destroy()

	def curRet(self, pair, err):
		if(err == 0):
			self.view.logs.insert(END, "Successfully marketed "+pair+".\n")
		elif(err == 1):
			self.view.logs.insert(END, "Couldn't find market for "+pair+".\n")
		elif(err == 2):
			self.view.logs.insert(END, "Error, couldn't market "+pair+" at less than -20% from highest bid. Please market manually.\n")
		elif(err == 3):
			self.view.logs.insert(END, "Unknown error while setting order for "+pair+".\n")
		self.root.update_idletasks()

	def config(self, event):
		self.confWin = Toplevel(self.root)
		self.confWin.grab_set()
		self.view.config(self.confWin, self.conf['apis'])
		self.view.saveButton.bind("<Button>", self.editApis)
		self.view.cancelButton.bind("<Button>", self.cancelEdit)

	def editApis(self, event):
		self.confWin.withdraw()
		for i in range(0, len(self.conf['apis'])):
			key = self.view.keyInput[i].get()
			secret = self.view.secretInput[i].get()
			if(key != self.conf['apis'][i]['ApiKey'] or secret != self.conf['apis'][i]['ApiSecret']):
				self.view.logs.insert(END, "Checking new "+self.conf['apis'][i]['Name']+" api key and secret ... ")
				self.root.update_idletasks()
				if(self.model.checkApi(self.conf['apis'][i]['Name'], key, secret)):
					self.conf['apis'][i]['ApiKey'] = key
					self.conf['apis'][i]['ApiSecret'] = secret
					self.view.logs.insert(END, "OK\n")
				else:
					self.view.logs.insert(END, "NOT OK\n")
		file = open("./config/apis.json", "w")
		file.write(json.dumps(self.conf))
		self.confWin.destroy()

	def cancelEdit(self, event):
		self.confWin.destroy()

def main():
	c = Controller()
	c.run()

if __name__ == "__main__":
	main()