from bridge import *

class Model():

	def getBridge(self, api, key, secret):
		if(isinstance(api, int) or api.isdigit()):
			api = api
		else:
			api = APIS.index(api)+1
		bridge = Bridge(api, key, secret)
		return bridge

	def connectBridge(self, conf):
		selectedApi = APIS.index(conf['selectedApi'])
		self.bridge = self.getBridge(selectedApi+1, conf['apis'][selectedApi]['ApiKey'], conf['apis'][selectedApi]['ApiSecret'])

	def checkApi(self, api, key, secret):
		bridge = self.getBridge(api, key, secret)
		return bridge.checkConnection()

	def getCryptos(self):
		return self.bridge.getCurrencyList()

	def panicSell(self, curs, cb):
		return self.bridge.marketCurrencies(curs, cb)