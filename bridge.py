import re, json, math
from bittrex_api.bittrex.bittrex import Bittrex
from poloniex_api.poloniex import Poloniex

BITTREX = 1
POLONIEX = 2
APIS = ["Bittrex", "Poloniex"]
MIN = [0.0007, 0.00013]

class Bridge():
	""" This class is a bridge for autolol between the different APIs"""

	def __init__(self, api, key, secret):
		self.api = api
		self.key = key
		self.secret = secret

		if(self.api == BITTREX):
			self.bittrex = Bittrex(key, secret)
		elif(self.api == POLONIEX):
			self.poloniex = Poloniex(key, secret)

	def checkConnection(self):
		if(self.api == BITTREX):
			return self.bittrex.get_balances()['success']
		elif(self.api == POLONIEX):
			try:
				self.poloniex.returnBalances()
				return True
			except:
				return False
		return None

	def getBalance(self):
		if(self.api == BITTREX):
			return self.bittrex.get_balances()['result']
		elif(self.api == POLONIEX):
			return self.poloniex.returnCompleteBalances()
		return None

	def getOpenOrders(self):
		if(self.api == BITTREX):
			return self.bittrex.get_open_orders()['result']
		elif(self.api == POLONIEX):
			return self.poloniex.returnOpenOrders()
		return None

	def cancelOrder(self, order):
		if(self.api == BITTREX):
			self.bittrex.cancel(order['OrderUuid'])
		elif(self.api == POLONIEX):
			self.poloniex.cancelOrder(order['orderNumber'])

	def getCurrencyList(self, pair=None):
		curs = self.getBalance()
		curTab = []
		if(self.api == BITTREX):
			summaries = self.bittrex.get_market_summaries()['result']
			for cur in curs:
				idx = next((idx for idx in summaries if idx['MarketName'] == "BTC-"+cur['Currency']), None)
				if idx and (idx['Bid']*cur['Balance']) > MIN[self.api-1]:
					curTab.append(cur['Currency'])
		elif(self.api == POLONIEX):
			summaries = self.poloniex.returnTicker()
			for cur in curs:
				if("BTC_"+cur in summaries and ((float)(summaries["BTC_"+cur]["highestBid"])*((float)(curs[cur]['available'])+(float)(curs[cur]['onOrders'])) > MIN[self.api-1])):
					curTab.append(cur)
		return curTab

	def cancelOrders(self, curs):
		open_orders = self.getOpenOrders()
		for cur in curs:
			if(self.api == BITTREX):
				it = (it for it in open_orders if re.sub(r'(.*)-(.*)', r'\2', it['Exchange']) == cur and it['OrderType'] == "LIMIT_SELL")
			elif(self.api == POLONIEX):
				it = (it2 for it in open_orders for it2 in open_orders[it] if re.sub(r'(.*)_(.*)', r'\2', it) == cur and it2['type'] == "sell")
			order = next(it, None)
			while(order != None):
				self.cancelOrder(order)
				order = next(it, None)

	def marketCurrencies(self, curs, cb):
		self.cancelOrders(curs)
		balance = self.getBalance()
		if(self.api == BITTREX):
			pairs = ["BTC-"+item for item in curs]
			summaries = self.bittrex.get_market_summaries()['result']
			for pair in pairs:
				idx = next((idx for idx in summaries if idx['MarketName'] == pair), None)
				if(idx == None):
					cb(pair, 1)
				else:
					price = idx['Bid']-idx['Bid']*0.2
					cur = pair.replace("BTC-","")
					avail = next((avail for avail in balance if avail['Currency'] == cur))
					cb(pair, self.marketCurrency(pair, avail['Available'], price))
		elif(self.api == POLONIEX):
			pairs = ["BTC_"+item for item in curs]
			summaries = self.poloniex.returnTicker()
			for pair in pairs:
				if(pair not in summaries):
					cb(pair, 1)
				else:
					price = (float)(summaries[pair]['highestBid']) - (float)(summaries[pair]['highestBid'])*0.2
					cur = pair.replace("BTC_","")
					avail = balance[cur]['available']
					if((float)(avail)*price < MIN[self.api-1]):
						cb(pair, 3)
						return
					cb(pair, self.marketCurrency(pair, avail, price))

	def marketCurrency(self, pair, avail, price):
		if(self.api == BITTREX):
			res = self.bittrex.sell_limit(pair, avail, price)
			if(res['success'] == False):
				print(json.dumps(res))
				return 3
			open_orders = self.bittrex.get_open_orders(pair)['result']
			if(next((it for it in open_orders if it['OrderType'] == "LIMIT_SELL"), None) != None):
				return 2
			return 0
		elif(self.api == POLONIEX):
			res = self.poloniex.sell(pair, price, avail)
			if("orderNumber" not in res):
				print(json.dumps(res))
				return 3
			if("resultingTrades" not in res):
				return 2
			amount = 0
			for trade in res["resultingTrades"]:
				amount += (float)(trade["amount"])
			if(math.isclose(amount, (float)(avail), rel_tol=1e-3) == False):
				return 2
			return 0