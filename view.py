from tkinter import *
from tkinter.ttk import *

class View():

	def __init__(self, root, conf):
		self.root = root
		self.selectedApi = StringVar()
		values = []
		for api in conf['apis']:
			values.append(api['Name'])
		cb = Combobox(root, textvariable=self.selectedApi, values=values, state='readonly')
		cb.set(conf['selectedApi'])
		cb.pack()
		self.sellButton = Button(self.root, text="Sell")
		self.sellButton.pack()
		self.configButton = Button(self.root, text="Configure")
		self.configButton.pack()
		self.logs = Text(root, height=10, width=200)
		scrollbar = Scrollbar(root)
		scrollbar.pack(side=RIGHT, fill=Y)
		scrollbar.config(command=self.logs.yview)
		self.logs.config(yscrollcommand=scrollbar.set)
		self.logs.bind("<<Modified>>", self.scrollDown)
		self.logs.pack()

	def scrollDown(self, *args):
		self.logs.see(END)
		self.logs.edit_modified(0)

	def config(self, win, apis):
		self.keyInput, self.secretInput = [], []
		note = Notebook(win)
		for api in apis:
			tab = Frame(note)
			note.add(tab, text=api['Name'])
			self.keyInput.append(Entry(tab))
			self.keyInput[-1].insert(END, api['ApiKey'])
			self.secretInput.append(Entry(tab))
			self.secretInput[-1].insert(END, api['ApiSecret'])
			Label(tab, text="ApiKey").pack()
			self.keyInput[-1].pack()
			Label(tab, text="ApiSecret").pack()
			self.secretInput[-1].pack()
			note.pack()
		self.saveButton = Button(win, text="Save")
		self.cancelButton = Button(win, text="Cancel")
		self.saveButton.pack()
		self.cancelButton.pack()

	def showBeforeSell(self, win, cryptos):
		self.checkBoxs = []
		for crypto in cryptos:
			var = BooleanVar()
			var.set(True)
			ck = Checkbutton(win, text=crypto, variable=var)
			ck.var = var
			ck.crypto = crypto
			ck.pack()
			self.checkBoxs.append(ck)
		self.sellButton = Button(win, text="Sell")
		self.cancelButton = Button(win, text="Cancel")
		self.sellButton.pack()
		self.cancelButton.pack()